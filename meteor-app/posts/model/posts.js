/**
 * Created by daniel on 2016-02-02.
 */
Posts = new Mongo.Collection('Posts');

Meteor.methods({
  addPost: function (data) {
    check(data, {
      content: String,
      author: {_id: String},
      topic: {_id: String}
    });

    var author = Authors.findOne({_id: data.author._id});
    var topic = Topics.findOne({_id: data.topic._id});

    if (!topic) {
      throw new Meteor.Error(404, "Topic doesn't exists");
    }

    if (author) {
      Posts.insert({
        content: data.content,
        author: {
          _id: author._id,
          username: author.username
        },
        created: new Date(),
        topic: {
          _id: topic._id
        }
      })
    } else {
      throw new Meteor.Error(404, "Author doesn't exists");
    }
    return true;
  },
  changeAuthor: function (data) {
    check(data, {
      _id: String,
      author: {
        _id: String
      }
    });

    var post = Posts.findOne({_id: data._id});
    var author = Authors.findOne({_id: data.author._id});

    if (!post) {
      throw new Meteor.Error(404, "Post doesn't exists");
    }

    if (!author) {
      throw new Meteor.Error(404, "Autor doesn't exists");
    }

    Posts.update({_id: data._id}, {$set: {author:{_id: author._id, username: author.username}}});

    return true
  },
  changeContent: function (data) {
    check(data, {
      _id: String,
      content: String
    });

    var post = Posts.findOne({_id: data._id});

    if (!post) {
      throw new Meteor.Error(404, "Post doesn't exist");
    }

    if (!data.content.length) {
      throw new Meteor.Error(400, "Post should have content");
    }

    Posts.update({_id: post._id}, {$set: {content: data.content}});
    return true
  },
  removePost: function (data) {
    check(data, {_id: String});

    var post = Posts.findOne({_id: data._id});
    if (!post) {
      throw new Meteor.Error(404, "Post doesn't exists");
    }

    Posts.remove({_id: data._id});
    return true;
  }

});
