"use strict";


var app = angular.module('LearnApp', ['angular-meteor',
  'ui.router',
  'LearnApp.topics',
  'LearnApp.posts']);


app.run(function ($rootScope) {
  $rootScope.$on('$stateChangeSuccess',function(){
    $("html, body").animate({ scrollTop: 0 }, 200);
  })
});
