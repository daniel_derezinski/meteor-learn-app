/**
 * Created by daniel on 2016-02-01.
 */
Meteor.startup(function () {
  var authorsCount = Authors.find().count();
  var f = faker;
  if (authorsCount < 5) {
    Authors.remove({});
    var authors = [
      {
        username: f.internet.userName(),
        email: f.internet.email(),
        joined: f.date.past()
      },
      {
        username: f.internet.userName(),
        email: f.internet.email(),
        joined: f.date.past()
      },
      {
        username: f.internet.userName(),
        email: f.internet.email(),
        joined: f.date.past()
      },
      {
        username: f.internet.userName(),
        email: f.internet.email(),
        joined: f.date.past()
      },
      {
        username: f.internet.userName(),
        email: f.internet.email(),
        joined: f.date.past()
      }
    ];

    authors.forEach(function (ele) {
      Authors.insert(ele);
    });

  }


  var topicsCount = Topics.find().count();

  if (topicsCount < 5) {
    Topics.remove({});
    var a = Authors.find().fetch();

    var topics = [
      {
        title: f.commerce.productName(),
        created: f.date.past(),
        author: {
          _id: a[0]._id,
          username: a[0].username
        }
      },
      {
        title: f.commerce.productName(),
        created: f.date.past(),
        author: {
          _id: a[1]._id,
          username: a[1].username
        }
      },
      {
        title: f.commerce.productName(),
        created: f.date.past(),
        author: {
          _id: a[2]._id,
          username: a[2].username
        }
      },
      {
        title: f.commerce.productName(),
        created: f.date.past(),
        author: {
          _id: a[3]._id,
          username: a[3].username
        }
      },
      {
        title: f.commerce.productName(),
        created: f.date.past(),
        author: {
          _id: a[4]._id,
          username: a[4].username
        }
      }
    ];
    
    topics.forEach(function (topic) {
      Topics.insert(topic);
    })
  }
  var postsCount = Posts.find().count();

  if (postsCount < 12) {
    Posts.remove({});
    a = Authors.find().fetch();
    var t = Topics.find().fetch();
    var posts = [
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[0]._id,
          username: a[0].username
        },
        topic: {
          _id: t[0]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[1]._id,
          username: a[1].username
        },
        topic: {
          _id: t[0]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[4]._id,
          username: a[4].username
        },
        topic: {
          _id: t[0]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[1]._id,
          username: a[1].username
        },
        topic: {
          _id: t[1]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[2]._id,
          username: a[2].username
        },
        topic: {
          _id: t[1]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[1]._id,
          username: a[1].username
        },
        topic: {
          _id: t[1]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[4]._id,
          username: a[4].username
        },
        topic: {
          _id: t[2]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[3]._id,
          username: a[3].username
        },
        topic: {
          _id: t[2]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[3]._id,
          username: a[3].username
        },
        topic: {
          _id: t[2]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[1]._id,
          username: a[1].username
        },
        topic: {
          _id: t[3]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[0]._id,
          username: a[0].username
        },
        topic: {
          _id: t[3]._id
        }
      },
      {
        content: f.lorem.paragraphs(),
        created: f.date.past(),
        author: {
          _id: a[4]._id,
          username: a[4].username
        },
        topic: {
          _id: t[3]._id
        }
      }
    ];

    posts.forEach(function (post) {
      Posts.insert(post);
    })

  }

});
