/**
 * Created by daniel on 2016-02-02.
 */

Meteor.publish('Posts', function (topicId) {
  if (!topicId) {
    return [];
  }
  var sel = {'topic._id': topicId},
    opt = {};

  return Posts.find(sel, opt);
});
