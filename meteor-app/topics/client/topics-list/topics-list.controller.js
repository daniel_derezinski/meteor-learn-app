/**
 * Created by daniel on 2016-02-01.
 */

var topicsModule = angular.module("LearnApp.topics");

topicsModule.controller('LearnApp.topics.topicsListCtrl', ['$scope', '$reactive', function ($scope, $reactive) {
  $reactive(this).attach($scope);

  this.subscribe('Topics');

  this.orderTitle = "";
  this.orderCreated = "";

  this.helpers({
    topics: function () {
      var titleOrd = this.getReactively('orderTitle');
      var createdOrd = this.getReactively('orderCreated');

      var sort = {};
      if (titleOrd === 'asc') {
        sort.title = 1
      } else if (titleOrd === 'desc') {
        sort.title = -1
      }

      if (createdOrd === 'asc') {
        sort.created = 1
      } else if (createdOrd === 'desc') {
        sort.created = -1
      }


      return Topics.find({}, {sort: sort });
    }
  })
}]);
