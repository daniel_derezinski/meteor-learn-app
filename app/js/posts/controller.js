/**
 * Created by daniel on 2016-01-29.
 */

var posts = angular.module('LearnApp.posts');

posts.controller("LearnApp.posts.postCtrl", ['$scope', '$routeParams', '$timeout',
  'LearnApp.RestService.Posts', "LearnApp.RestService.Authors",
  function ($scope, $routeParams, $timeout, restPosts, restAuthors) {
    $scope.posts = [];
    $scope.authors = [];

    restPosts.doTopicPosts($routeParams).$promise.then(function (response) {
      if (response.$resolved) {
        $scope.posts = response.data;
      } else {
        console.log('response not resolverd', response);
      }
    }, function (response) {
      console.log('error geting posts', response);
    });

    restAuthors.doAuthorsList().$promise.then(function (response) {
      if (response.$resolved) {
        $scope.authors = response;
        $scope.newPost.author = $scope.authors[0].id;
      }
    });

    $scope.newPost = {
      author: 1,
      content: ""
    };

    $scope.savePost = function () {
      $scope.showSave = false;
      $scope.saveFadeOut = false;
      if ($scope.createPost.$valid) {
        var postToSave = {
          topic: {
            id: parseInt($routeParams.id)
          },
          created: new Date(),
          content: $scope.newPost.content,
          author: {
            id: $scope.newPost.author,
            username: _.find($scope.authors, {id: $scope.newPost.author}).username
          }
        };

        restPosts.createPost(postToSave).$promise.then(function (response) {
          if (response.$resolved) {
            postToSave.id = response.id;
            console.log($scope.posts.length);
            $scope.posts.push(postToSave);
            console.log($scope.posts.length);
            $scope.newPost.content = "";
            $scope.showSave = true;
            $timeout(function () {
              $scope.saveFadeOut = true;
            }, 1000);
          }
        }, function (response) {
          console.log('error', response);
        });
      } else {
        console.log('form not valid');
      }
    };

    $scope.deletePost = function (id) {
      restPosts.deletePost({id: id}).$promise.then(function (response) {
        if (response.$resolved) {
          if (response.count > 0) {
            _.remove($scope.posts, {id: id})
          } else {
            console.log('error api not deleted')
          }
        }
      }, function (response) {
        console.log('delete error', response);
      })
    };

    $scope.editAuthor = function (post) {
      console.log('author', post.author);
      post.author.username = _.find($scope.authors, {id: post.author.id}).username;
    };

    $scope.editPost = function (post, form) {
      $scope.showSave = false;
      $scope.saveFadeOut = false;
      if (form.$valid) {
        restPosts.editPost({id:post.id}, post).$promise.then(function (response) {
          if (response.count > 0) {
            $scope.showSave = true;
            $timeout(function () {
              $scope.saveFadeOut = true;
            }, 1000);
            console.log('post edited', response);

          } else {
            console.log('edit error', response);
          }
        },function (response){
          console.log('edit error', response);
        });

      } else {
        console.log('form not valid');
      }

    };

}]);
