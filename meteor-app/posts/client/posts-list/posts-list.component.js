/**
 * Created by daniel on 2016-02-02.
 */
var postsModule = angular.module("LearnApp.posts");

postsModule.directive('learnPosts', function () {
  return {
    restrict: 'E',
    templateUrl: 'posts/client/posts-list/posts-list.html',
    controllerAs: 'postsCtrl',
    controller: ['$scope', '$stateParams', '$reactive' ,function ($scope, $stateParams, $reactive) {
      $reactive(this).attach($scope);

      var topicId = $stateParams._id;
      var self = this;

      this.subscribe("Posts", function () {
        return [topicId]
      });

      this.createPost = $scope.createPost;

      this.subscribe("Authors", function () {
        return []
      }, {
        onReady: function () {
          self.newPost.author._id = Authors.findOne()._id;
        }
      });

      this.newPost = {
        content: "",
        author: {
          _id: ""
        }
      };

      
      this.helpers({
        posts: function () {
          return Posts.find({'topic._id': topicId});
        },
        authors: function () {
          return Authors.find({}).fetch();
        }
      });
      
      this.addPost = function () {
        this.newPost.topic = {_id: topicId};
        console.log(this.newPost);
        Meteor.call('addPost', this.newPost, function (error, result) {
          if (result) {
            console.log('Post added');
            self.newPost = {
              content: "",
              author: {
                _id: Authors.findOne()._id
              }
            };
          } 
          
          if (error) {
            console.log("Error adding post", error);
          }
        });
      };
      
      this.changeAuthor = function (post) {
        var data = {_id: post._id,
          author: {
            _id: post.author._id
          }
        };
        
        Meteor.call('changeAuthor', data, function (error, result) {
          if (result) {
            console.log("Author changed");
          }
          
          if (error) {
            console.log('Error change author', error);
          }
        })
      };

      this.changeContent = _.debounce(function (post) {
        var data = {
          _id: post._id,
          content: post.content
        };
        
        Meteor.call("changeContent", data, function (error, result)  {
          if (result) {
            console.log("Content changed");
          } 
          if (error) {
            console.log('Error changing content', error);
          }
        })

      }, 500);
      
      this.removePost = function (_id) {
        var data = {_id : _id};
        
        Meteor.call('removePost', data, function (error, result) {
          if (result) {
            console.log("Post removed");
          }
          
          if (error) {
            console.log("Error remove post", error);
          }
        })
      }
    
    }]
  }
});
