/**
 * Created by daniel on 2016-02-02.
 */
var app = angular.module('LearnApp');

app.config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
  $locationProvider.html5Mode(true);

  $stateProvider
    .state('topics', {
      url: '/topics',
      templateUrl: 'topics/client/topics-list/topics-list.html'
    })
    .state('topic', {
      url: '/topics/:_id',
      templateUrl: 'topics/client/topic/topic.html'
    });

  $urlRouterProvider.otherwise('/topics');
})
