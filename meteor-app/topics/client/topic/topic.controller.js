/**
 * Created by daniel on 2016-02-02.
 */

var topicsModule = angular.module("LearnApp.topics");

topicsModule.controller('LearnApp.topics.topicCtrl', ['$scope', '$reactive', '$stateParams',
  function ($scope, $reactive, $stateParams) {
    $reactive(this).attach($scope);

    var topicId = $stateParams._id;

    this.subscribe('Topics', function () {
      return [topicId]
    });

    this.helpers({
      topic: function () {
        return Topics.findOne({_id: topicId});
      }
    })
}]);
