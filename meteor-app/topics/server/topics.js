/**
 * Created by daniel on 2016-02-02.
 */

Meteor.publish('Topics', function (_id) {
  var sel = {},
    opt = {};

  if (_id) {
    sel = {_id: _id};
  }

  return Topics.find(sel, opt)
});

Topics.allow({
  insert: function () {
    return false;
  },
  update: function () {
    return false;
  },
  remove: function () {
    return false;
  }
});
