/**
 * Created by daniel on 2016-01-29.
 */
var posts = angular.module("LearnApp.posts");

posts.directive('learnPost', function () {
  return {
    restrict: 'E',
    scope: false,
    templateUrl: '/app/js/posts/partials/post.html'
  }
});
